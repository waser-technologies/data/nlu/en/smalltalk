.PHONY: install_requirements, validate, train, shell, serve

install_requirements:
	@pip install -r requirements.txt

validate:
	@rasa data validate --data ./data/ -d ./domain.yml -c ./config.yml

train:
	@rasa train --data ./data/ -d ./domain.yml -c ./config.yml --out ./models

shell:
	@rasa shell -m models/ --enable-api --endpoints ./endpoints.yml --credentials ./credentials.yml

serve:
	@rasa run actions & rasa run -m models/ --enable-api --endpoints ./endpoints.yml --credentials ./credentials.yml