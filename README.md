# Smalltalk: NLU Domain

Smalltalk is a domain specific dataset aimed at standardizing intent classes for general understanding of language.


## Installation

Use the [Domain Management Tool](https://gitlab.com/waser-technologies/technologies/dmt) to effortlessly install, validate and train a new NLP model using this domain.

```zsh
dmt -V -T -a https://gitlab.com/waser-technologies/data/nlu/en/smalltalk.git
```

## Usage

To test the model on this domain, use the `dmt` to serve the latest model.

```zsh
dmt -S
```
This will take a while to load. 

Once you see:
> `INFO     root  - Rasa server is up and running.`

You can query the server in another terminal.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "Hello", "sender": "user" }'
```

Or simply use [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant Hello
```

## Is there pre-trained models of this domain?

Yes. I train and distribute models using all the domains I've made public for a particular language.

You can download the latest pre-trained models for english NLU under [Models/English/NLU](https://gitlab.com/waser-technologies/models/en/nlu).


## Data structure

You'll find the dataset inside `data/`.

### Intent Classification & Entity Recognition

Under `data/nlu/`, you'll find everything related to intent classes and entities extraction.

```zsh
.
├── data/
│   ├── nlu/
│   │   ├── *.yml
│   │   └── verbs.yml
```

> `verbs.yml`
> is the principal file.
> It contains *intents* for *verbs*.

> `*.yml`
> -> When a *verb* is complex enough it is moved to its own file.

> `/!\` For each *verb* with its own file, you should create a [ResponseSelector](https://rasa.com/docs/rasa/components#responseselector) for the *verb*.

#### *Intent*:

A *verb*, its *target*, its *subject*, its *target' subject*.

> But it's pronounced *target' subject* *target* *subject* *verb*.

```yml
#!data/nlu/can.yml

version: "3.1"
nlu:
# talk you cant
- intent: can_not/talk_you
  examples: |
    - You can't talk
    - You don't have the right to talk
    - Don't talk
    - silent
    - silence
```

```yml
#!data/nlu/imagine.yml

version: "3.1"
nlu:
# intent: <VERB>(/<VERB_TARGET>)(_<VERB_SUBJECT>)(_<VERB_TARGET_SUBJECT>)
- intent: imagine/look_you_me
# To better understand an intent, try say'ng it like this:
# <VERB_TARGET_SUBJECT> <VERB_TARGET> <VERB_SUBJECT> <VERB>
# me look you imagine
# Or if you prefer: My look, you imagine.
  examples: |  
    - what do you think I look like
    - imagine how I look
```

#### *Entity*:

One good *slot* filler tool.

Enities are wraped in brackets and followed by their type in parenthesys.

```yml
#!data/nlu/speak.yml

version: "3.1"
nlu:
# languages which you speak (remember, target' subject, target, verb' subject and verb)
- intent: speak/which_you_languages
  examples: |
    - Do you speak [german](language)?
    - Do you understand [spanish](language)?
```

```yml
#!data/nlu/verbs.yml

version: "3.1"
nlu:
# This is a special intent to input data like your name, an amount of money, dates, etc.
- intent: enter_data
  examples: |
    - I am [Christina](first_name) [Sullivan](last_name)
    - I work as a [frontend dev](job_function)
    - I work for the [New York Times](organisation)
    - get [dates](entity) from messages
    - how much [money](entity)

```

#### *Response*:

Utterance that answers an *intent*.

```zsh
.
├── data/
│   ├── nlu/
│   │   ├── responses/
│   │   │   ├── *.yml
│   │   │   └──responses.yml
```

> `responses.yml`
> is the principal file.
> It contains *responses* to *intents*.

> `*.yml`
> -> There is a *response* file for every *verb* that has its own file.

> `(i)`
> The *response* file is named after the *verb* it responds to.
> (e.g. : `data/nlu/be.yml` -> `data/nlu/responses/be.yml`)

A *response* is a basic action that return static text based on the intent.

It always starts with `utter_` followed by the name of the *intent* it responds to.

Therefor `utter` becomes `respond`.

Which gives: `respond {intent}`.

Here is a concrete example extracted from `apologize`.

```yml
#!data/nlu/apologize.yml


version: "3.1"
nlu:
# Intent: you apologize
- intent: apologize/me_you
  examples: |
    - excuse you
    - watch yourself
    - apologise
    - apologize now
    - redeem yourself
    - apologies to me
# Intent: me apologize
- intent: apologize/you_me
  examples: |
    - excuse me
    - I apologize
    - sorry
    - I'm sorry
```


```yml
#!data/nlu/responses/apologize.yml

version: "3.1"
responses:
# Action: respond apologize
# Here the intent only contains a *verb*.
# It should be understood as an injuction.
# e.g.: 'Apologise, now!'
  utter_apologize:
  - text: My apologies.
  - text: I'm deeply sorry.
  - text: I am very sorry.
  - text: My excuses
  - text: I would like you to pardon me
  - text: Pardon me.
  - text: You should know that I'm sorry.

# Action: respond me you apologize
# Here *me* wants *apologize* from *you*.
# Which it's basically the same as saying *apologize*.
# We won't repeat ourselfs,
# we'll use the default response of this verb instead.
# e.g.: 'Excuse you!'
  utter_apologize/me_you:
  - template: utter_apologize

# Action: respond you me apologize
# Here the intent describes *me* *apologize* to *you*.
# It should be understood as an excuse from *me*.
# e.g.: 'Sorry'
  utter_apologize/you_me:
  - text: I pardon you.
  - text: You are excused.
  - text: Well, please be mindful.
  - text: I do.
  - text: Let's not talk about it any further.
  - text: I don't need to ear it.
  - text: No need, really.
  - text: You don't need to say it.
  - text: Forget about it!
```

### *Stories*:

Readme a *story*

```zsh
.
├── data/
│   ├── stories/
│   │   ├── *.yml
│   │   └── stories.yml
```

```yml
#!data/stories/stories.yml

version: "3.1"
stories:
- story: user is glad to meet assistant
  steps:
  - intent: greet # Hi
  - action: utter_greet # Hey
  - intent: glad # Nice to meet you
  - action: utter_glad # It my pleasure really
```
